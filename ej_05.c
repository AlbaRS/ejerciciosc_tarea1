#include <stdio.h>

//Pedir radio y calcular diametro, circunferencia y area.

int main(){
    //declaracion de floats
    float radio = 0;
    float diametro = 0;
    float circunferencia = 0;
    float area = 0;
    float pi = 3.14;
    
    printf("CALCULADOR DE DIAMETRO, CIRCUMFERENCIA Y AREA \n\n");
    printf("Introduce el radio del circulo: \n");
    scanf("%f", &radio);
    getchar();
    //calculos para cada cosa
    diametro = radio*2;
    circunferencia = pi * diametro;
    area = pi * (radio * radio);
    
    //imprimimos los resultados
    //limite de dos decimales en floats
    printf("DIAMETRO: %4.2f\n", diametro);
    printf("CIRCUNFERENCIA: %4.2f\n", circunferencia);
    printf("AREA: %4.2f\n", area);
    
    getchar();
    return 0;

}