#include <stdio.h>

//Pedir dos angulos y encontrar el tercero.
int main(){
    
    //declaramos todas las variables
    float angulo1 = 0;
    float angulo2 = 0;
    float angulo3 = 0;
    float dosangulos = 0;
    float total=180;
    float tresangulos = 0;
    
    //pedimos los angulos
    printf("Introduce los dos angulos: \n");
    scanf("%f %f", &angulo1, &angulo2);
    getchar();
    printf("\nAngulos declarados: %3.2f, %3.2f", angulo1, angulo2);
    //calculos para obtener el que necesitamos
    dosangulos = angulo1+angulo2;
    angulo3 = total - dosangulos;
    //para comprobar que el total será 180
    tresangulos = angulo3 + dosangulos;
    
    //Si dos o tres angulos en total superan 180, se imprimira como que esta mal
    if ((tresangulos > total) || (dosangulos>=total)){
        printf("\nHas introducido angulos mal, la suma es igual o mas de 180 grados");
    }else if (tresangulos==total){//si los tres angulos dan el total, esta bien
        printf("\nEl tercer angulo es %3.2f", angulo3);
        }
    
    getchar();
    return 0;

}