#include <stdio.h>

//Pedir segundos y convertirlos en minutos y horas.

int main(){
    //declaramos variable
    int segundos = 0;
    
    printf("CALCULADOR DE SEGUNDOS A MINUTOS Y HORAS \n\n");
    printf("Introduce los segundos: \n");
    scanf("%i", &segundos);
    getchar();
    
    //como en el ej anterior, hago los calculos con segundos
    printf("MINUTOS: %d\n", segundos/60);
    printf("HORAS: %d\n", segundos/3600);
    printf("DIAS: %d\n", segundos/86400);
    
    getchar();
    return 0;

}