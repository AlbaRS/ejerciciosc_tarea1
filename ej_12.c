#include <stdio.h>
#include <stdlib.h>

//Pedir 10 numeros y mostar la suma y el producto (CON WHILE)

int main () {
    //declaramos variables
   int sumatotal = 0;
   int multiplicaciontotal = 1;
   int posicion = 0;
   int numero;

   
   while (posicion<10){//mientras la posicion sea menor a 10 sigue pidiendo numero 
       printf("Dame un numero:");
       scanf("%d",&numero);
       getchar();
        //los numeros se van sumando o multiplicando en las variables
       sumatotal+=numero;
       multiplicaciontotal+=numero;
       posicion++;//vamos sumando la posicion segun pedimos numero
   }
   
   //con for
   /*for(int i=0;i<10;i++){
       printf("Dame un numero\n");
       scanf("%d",&tmp);
       getchar();
       
       numeros[i] = tmp;  
   }
   
   for(int i=0;i<10;i++){
       sumatotal += numeros[i];
       multiplicaciontotal *= numeros[i];     
   }*/
   
   
   printf("La suma total es %d\n La multiplicacion total es %d\n",sumatotal,multiplicaciontotal);
   
   getchar();
   return(0);
}