#include <stdio.h>
#include <stdlib.h> // Librería para el random


//Imprimir 5 nums random.

int main(){
    srand(time(NULL)); //Para que nos de distintos numeros cada vez que iniciemos el programa, si no dan siempre los mismos
    printf("NUMEROS LOTERIA: \n");
    for (int i=0; i<=5; i++){//bucle para los 5 nums
        printf("%d\n", rand()%100); //El 100 para que nos de numeros dentro de ese rango (0 a 99) o salen numeros muy altos
    }
    
    getchar();
    return 0;

}