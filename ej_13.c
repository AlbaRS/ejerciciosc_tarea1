#include <stdio.h>
#include <stdlib.h>

//Pedir secuencia de numeros y enseñar la suma de todos
//cuando se introduzca un numero menor o igual a 0, para

int main () {
    //declaramos variables
   int posicion;
   int sumatotal=0;
    //pedimos el numero
    printf("Dame un numero\n");
    scanf("%d",&posicion);
    getchar();
    
    //bucle que mientras el numero sea mayor que 0, siga pidiendo el numero
   while(posicion>=0){
       sumatotal+=posicion;
       printf("Dame otro numero\n");
       scanf("%d",&posicion);
       getchar();
   }
    //una vez pare, nos enseñara la suma total
   printf("Sumatotal = %d\n",sumatotal);

   getchar();
   return(0);
}