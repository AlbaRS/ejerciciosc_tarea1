#include <stdio.h>

//Convertir de grados a fahrenheit.
int main(){
    //declaramos variables
    float grados = 0;
    float fahr = 0;
    
    printf("Introduce los grados: \n");
    scanf("%f", &grados);
    getchar();
    //calculo de grados a fahrenheit
    fahr = (grados * 1.8)+32;
    
    printf("\nFahrenheit: %4.2f", fahr);
    getchar();
    
    return 0;

}