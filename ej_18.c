#include <stdio.h>

//Pedir numero y decir si es negativo (con funcion).

//funcion si numero es menor que 0 es negativo
int IsNegative(int num){
        if (num <0){ //condicion
            return 'n'; //return n si es negativo
        }else {
            return 'p';//return p si es positivo
            }
    }

int main(){
    int numero = 0;
    int funcion;
    
    printf("Introduce un numero: \n");//pegimos numero y lo guardamos en la variable numero
    scanf("%i", &numero);
    getchar();
    
    funcion=IsNegative(numero);//llamamos a la funcion entrando la variable numero
    
    if (funcion == 'p'){//si la funcion nos devuelve el número como p es positivo
        printf("\nEs positivo.");
    }else{//si no, sera negativo
        printf("\nEs negativo.");
    }
    
    getchar();
    return 0;

}