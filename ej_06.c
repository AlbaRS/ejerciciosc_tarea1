#include <stdio.h>

//Pedir centimetros y convertirlos en metros y kilometros.

int main(){
    //declaramos variables
    float cm = 0;
    
    //pedimos el numero
    printf("CALCULADOR DE CENTIMETROS A METROS Y KM \n\n");
    printf("Introduce los centimetros: \n");
    scanf("%f", &cm);
    getchar();
    
    //imprimimos con la operacion ya hecha
    //pero se podria declarar en otras variables
    //por ejemplo metros = cm/100;
    //km = cm/100000
    //km = metros/1000;
    //En caso de que no nos pidieran los metros,
    //ya tenemos la operacion hecha para que lo calcule desde los centimetros
    printf("METROS: %4.2f\n", cm/100);
    printf("KILOMETROS: %4.4f\n", cm/100000);
    
    getchar();
    return 0;

}