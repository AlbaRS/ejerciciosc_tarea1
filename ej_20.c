#include <stdio.h>
#include <stdlib.h>

//Crear calculadora.

int main(){
    //declaramos con decimales por si se quieren hacer operaciones con ellos
    float a = 0;
    float b = 0;
    float resultado = 0;
    char opcion;
    
    do{//mientras no se escoja la opcion "s", que haga esto todo el rato
        printf("\nIntroduce un numero: \n");
        scanf("%f", &a);
        getchar();
        
        //preguntamos que opcion queremos realizar
        //he introducido una de salida para cerrar el programa o nunca para
        printf("Que opercion vas a realizar? Escribir:\n'+' -> Suma\n '-' -> Resta\n'*' -> Multiplicacion\n '/' -> Division\n's' o 'S' -> Salir del programa\n");
        scanf("%c", &opcion);
        getchar();
        //si se escoge s o S cierra el programa 
        if (opcion == 's' || opcion == 'S'){
            return 0;
        }
        //pedimos el otro numero
        printf("\nIntroduce otro numero: \n");
        scanf("%f", &b);
        getchar();
        
        //limpiamos consola para que no se acumule tanto numero
        system("cls");
        //opciones con las operaciones
        switch (opcion){
            case '+':
                resultado = a + b;
                printf("\nSUMA\n %4.2f + %4.2f =  %4.2f", a, b, resultado);
                break;
            case '-':
                resultado = a - b;
                printf("\nRESTA\n %4.2f - %4.2f =  %4.2f", a, b, resultado);
                break;
            case '*':
            resultado = a * b;
                printf("\nMULTIPLICACION\n %4.2f * %4.2f =  %4.2f", a, b, resultado);
                break;
            case '/':
                resultado = a / b;
                printf("\nDIVISION\n %4.2f / %4.2f =  %4.2f", a, b, resultado);
                break;
            }
    }while (opcion != 's' || opcion != 'S');//la condicion para que el do siga o para el programa
    
    getchar();
    return 0;

}